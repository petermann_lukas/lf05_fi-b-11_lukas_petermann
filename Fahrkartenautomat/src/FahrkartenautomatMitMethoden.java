import java.util.Scanner;

public class FahrkartenautomatMitMethoden {

	public static Scanner tastatur = new Scanner(System.in);
	public static void main(String[] args) {

		while (true) {
			double zuZahlenderBetrag = FahrkartenbestellungErfassen();
			double eingezahlterGesamtbetrag = FahrkartenBezahlen(zuZahlenderBetrag);
			FahrkartenAusgeben();
			RueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

			tastatur.close();
		}
	}

	public static double FahrkartenbestellungErfassen() {

		boolean mehrfachAuswahl = true;
		double zuZahlenderBetrag = 0;

		while (mehrfachAuswahl) {

			System.out.println("Wählen Sie Ihre Wunschfahrkarte für AB Berlin aus:\n");
			System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
			System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
			System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23, 50 EUR] (3)");

			int auswahl = tastatur.nextInt();

			switch (auswahl) {
				case 1:
					System.out.println("Ihre Auswalhl: 1");
					zuZahlenderBetrag += 2.90;
					break;

				case 2:
					System.out.println("Ihre Auswalhl: 2");
					zuZahlenderBetrag += 8.60;
					break;

				case 3:
					System.out.println("Ihre Auswalhl: 3");
					zuZahlenderBetrag += 23.50;
					break;

				default:
					System.out.println("Ungültige Auswahl. Bitte entscheiden Sie sich für 1, 2 oder 3");
					break;
			}

			System.out.println("Zwischensumme: " + zuZahlenderBetrag + " €");

			System.out.println("Wollen sie weitere Fahrkarten buchen ?");
			System.out.println("JA (1)  NEIN (2)");

			int jaNein = tastatur.nextInt();

			if(jaNein == 2){
				mehrfachAuswahl = false;
			}
		}
		return zuZahlenderBetrag;
	}

	public static double FahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		return eingezahlterGesamtbetrag;
	}

	public static void FahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void RueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rueckgabebetrag > 0.0) {
			System.out.println("Der R�ckgabebetrag in H�he von " + rueckgabebetrag + " EURO");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
				"vor Fahrtantritt entwerten zu lassen!\n" +
				"Wir wuenschen Ihnen eine gute Fahrt. \n \n");

		System.out.println("--------------------------------------------\n\n");
	}

}
