import java.util.ArrayList;
import java.util.Scanner;


public class Main {

	public static void main(String[] args) {

		int geschlecht;
		int koerpergroesse;
		int koerpergewicht;
		double bmi;
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("W�hlen sie ihr Geschlecht.");
		System.out.println("");
		System.out.println("M�nnlich [1]");
		System.out.println("Weiblich [2]");
		System.out.println("");
		
		geschlecht = scanner.nextInt();
		
		if(geschlecht == 1) {
			
			System.out.print("Geben sie ihr K�rpergewicht in Kg (Ganzzahl) ein: ");
			koerpergewicht = scanner.nextInt();
			
			System.out.print("Geben sie ihre K�rpergr��e in cm ein: ");
			koerpergroesse = scanner.nextInt();
			
			bmi = koerpergewicht / ((koerpergroesse / 100) * (koerpergroesse / 100)) ;
			
			if(bmi < 20) {
				
				System.out.println("Laut BMI haben sie Untergewicht.");
			}
			else if(bmi > 19 && bmi < 26) {
				
				System.out.println("Laut BMI haben sie Normalgewicht.");
			}
			else if(bmi > 25) {
				
				System.out.println("Laut BMI haben sie �bergewicht");
			}
			
		}
		
		if(geschlecht == 2) {
			
			System.out.print("Geben sie ihr K�rpergewicht in Kg (Ganzzahl) ein: ");
			koerpergewicht = scanner.nextInt();
			
			System.out.println("Geben sie ihre K�rpergr��e in cm ein: ");
			koerpergroesse = scanner.nextInt();
			
			bmi = koerpergewicht / ((koerpergroesse / 100) * (koerpergroesse / 100)) ;
			
			if(bmi < 19) {
				
				System.out.println("Laut BMI haben sie Untergewicht.");
			}
			else if(bmi > 18 && bmi < 25) {
				
				System.out.println("Laut BMI haben sie Normalgewicht.");
			}
			else if(bmi > 24) {
				
				System.out.println("Laut BMI haben sie �bergewicht");
			}
			
		}
		scanner.close();


		System.out.println("");
		System.out.println("");
		System.out.println("*****************************************");
		System.out.println("* Klassifikation | M�nnlich  | Weiblich *");
		System.out.println("*****************|***********|***********");
		System.out.println("* Normalgewicht  |   < 20    |   < 19   *");
		System.out.println("*****************|***********|***********");
		System.out.println("* �bergewicht    |  20 - 25  |  19 - 24 *");
		System.out.println("*****************************************");
	}

}
