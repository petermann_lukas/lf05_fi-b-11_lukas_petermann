public class Ladung {

    //#region Properties
    private String bezeichnung;
    private int menge;
    //#endregion


    //#region Konstruktoren
    public Ladung(){

    }

    public Ladung(String ladungsBezeichnung, int ladungsMenge){
        this.bezeichnung = ladungsBezeichnung;
        this.menge = ladungsMenge;
    }

    /**
     * @return String
     */
    //#endregion


    //#region Getter & Setter
    public String getBezeichnung(){
        return this.bezeichnung;
    }

    /**
     * @param neueBezeichnung
     */
    public void setBezeichnung(String neueBezeichnung){
        this.bezeichnung = neueBezeichnung;
    }


    /**
     * @return int
     */
    public int getMenge(){
        return this.menge;
    }

    /**
     * @param neueMenge
     */
    public void setMenge(int neueMenge){
        this.menge = neueMenge;
    }
    //#endregion
}
