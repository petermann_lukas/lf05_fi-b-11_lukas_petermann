import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {

    //#region Properties
    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsname;
    //#endregion


    //#region ArrayLists
    private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
    private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
    //#endregion


    //#region Konstruktoren
    public Raumschiff(){

    }

    public Raumschiff(int torpedoAnzahl, int zustandEnergieversorgungInProzent,
                      int zustandSchildeInProzent, int zustandHuelleInProzent,
                      int zustandLebenserhaltungssystemeInProzent, int anzahlAndroiden,
                      String name){

        this.photonentorpedoAnzahl = torpedoAnzahl;
        this.energieversorgungInProzent = zustandEnergieversorgungInProzent;
        this.schildeInProzent = zustandSchildeInProzent;
        this.huelleInProzent = zustandHuelleInProzent;
        this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
        this.androidenAnzahl = anzahlAndroiden;
        this.schiffsname = name;
    }

    /**
     * @return int
     */
    //#endregion


    //#region Getter & Setter
    public int getPhotonentorpedoAnzahl(){
        return this.photonentorpedoAnzahl;
    }

    /**
     * @param neueAnzahl
     */
    public void setPhotonentorpedoAnzahl(int neueAnzahl){
        this.photonentorpedoAnzahl = neueAnzahl;
    }


    /**
     * @return int
     */
    public int getEnergieversorgungInProzent(){
        return this.energieversorgungInProzent;
    }

    /**
     * @param neuerProzentsatz
     */
    public void setEnergieversorgungInProzent(int neuerProzentsatz){
        this.energieversorgungInProzent = neuerProzentsatz;
    }


    /**
     * @return int
     */
    public int getSchildeInProzent(){
        return this.schildeInProzent;
    }

    /**
     * @param neuerProzentsatz
     */
    public void setSchildeInProzent(int neuerProzentsatz){
        this.schildeInProzent = neuerProzentsatz;
    }


    /**
     * @return int
     */
    public int getHuelleInProzent(){
        return this.huelleInProzent;
    }

    /**
     * @param neuerProzentsatz
     */
    public void setHuelleInProzent(int neuerProzentsatz){
        this.huelleInProzent = neuerProzentsatz;
    }


    /**
     * @return int
     */
    public int getLebenserhaltungssystemeInProzent(){
        return this.lebenserhaltungssystemeInProzent;
    }

    /**
     * @param neuerProzentsatz
     */
    public void setLebenserhaltungssystemeInProzent(int neuerProzentsatz){
        this.lebenserhaltungssystemeInProzent = neuerProzentsatz;
    }


    /**
     * @return int
     */
    public int getAndroidenAnzahl(){
        return this.androidenAnzahl;
    }

    /**
     * @param neueAnzahl
     */
    public void setAndroidenAnzahl(int neueAnzahl){
        this.androidenAnzahl = neueAnzahl;
    }


    /**
     * @return String
     */
    public String getSchiffsname(){
        return this.schiffsname;
    }

    /**
     * @param neuerName
     */
    public void setSchiffsname(String neuerName){
        this.schiffsname = neuerName;
    }

    /**
     * @param neueLadung
     */
    //#endregion


    //#region Methoden
    public void addLadung(Ladung neueLadung){
        ladungsverzeichnis.add(neueLadung);
    }

    public void ladungsverzeichnisAusgeben(){
        System.out.println("<<<<<<< LADUNGSVERZEICHNIS >>>>>>>");
        System.out.println();

        for (Ladung ladung : ladungsverzeichnis) {
            System.out.println("****************************");
            System.out.println("Ladung: " + ladung.getBezeichnung());
            System.out.println("Menge : " + ladung.getMenge());
            System.out.println("****************************");
        }

        System.out.println();
        System.out.println("<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>");
    }

    public void ladungsverzeichnisAufraeumen(){
        for (Ladung ladung : ladungsverzeichnis){
            if (ladung.getMenge() <= 0){
                ladungsverzeichnis.remove(ladung);
            }
        }

    }


    /**
     * @param raumschiff
     */
    public void photonentorpedoSchiessen(Raumschiff raumschiff){
        if (this.photonentorpedoAnzahl > 0){
            nachrichtAnAlle("Photonentorpedo abgefeuert!");
            photonentorpedoAnzahl --;
            treffer(raumschiff);
        }
        else
        {
            nachrichtAnAlle("-=*Click*=-");
        }
    }


    /**
     * @param raumschiff
     */
    public void phaserkanoneSchiessen(Raumschiff raumschiff){
        if (this.energieversorgungInProzent >= 50){
            nachrichtAnAlle("Phaserkanone abgefeuert!");
            this.energieversorgungInProzent -= 50;
            treffer(raumschiff);
        }
        else
        {
            nachrichtAnAlle("-=*Click*=-");
        }
    }


    /**
     * @param raumschiff
     */
    private void treffer(Raumschiff raumschiff){
        System.out.println("<<<<<<<<<<<< TREFFER >>>>>>>>>>>>");
        System.out.println();
        System.out.println(raumschiff.getSchiffsname() + " wurde getroffen!");
        System.out.println();
        System.out.println("<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>");

        if (raumschiff.getSchildeInProzent() > 0){
            raumschiff.setSchildeInProzent(raumschiff.getSchildeInProzent() - 50);
        }
        else
        {
            raumschiff.setHuelleInProzent(getHuelleInProzent() - 50);
            raumschiff.setEnergieversorgungInProzent(getEnergieversorgungInProzent() - 50);
        }

        if (raumschiff.getHuelleInProzent() <= 0){
            nachrichtAnAlle("Die Lebenserhaltungssysteme von " + raumschiff.getSchiffsname() + " wurden zerstört!");
        }
    }


    /**
     * @param nachricht
     */
    public void nachrichtAnAlle(String nachricht){
        System.out.println("<<<<<<< NACHRICHT AN ALLE >>>>>>>");
        System.out.println();
        System.out.println(nachricht);
        System.out.println();
        System.out.println("<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>");

        broadcastKommunikator.add(nachricht);
    }


    /**
     * @return ArrayList<String>
     */
    public ArrayList<String> eintraegeLogbuchZurueckgeben(){
        return broadcastKommunikator;
    }

    public void photonentorpedosLaden(){
        int eingesetzteTorpedos = 0;
        for (Ladung ladung : ladungsverzeichnis) {

            if (ladung.getBezeichnung() == "Photonentorpedo" && ladung.getMenge() > 0){
                this.photonentorpedoAnzahl++;
                eingesetzteTorpedos++;
                ladungsverzeichnis.remove(ladung);
            }
            else{
                System.out.println("Keine Photonentorpedos gefunden!");
                nachrichtAnAlle("-=*Click*=-");
            }
        }

        System.out.println(eingesetzteTorpedos + " Photonentorpedos wurden geladen!");
        eingesetzteTorpedos = 0;
    }


    /**
     * @param schutzschilde
     * @param energieversorgun
     * @param schiffshuelle
     * @param anzahlAndroiden
     */
    public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgun, boolean schiffshuelle, int anzahlAndroiden){
        int trueSchiffsstrukturen = 0;
        int ergebnis;

        if (schutzschilde){
            trueSchiffsstrukturen++;
        }
        if (energieversorgun){
            trueSchiffsstrukturen++;
        }
        if (schiffshuelle){
            trueSchiffsstrukturen++;
        }

        if (anzahlAndroiden > this.androidenAnzahl){
            ergebnis = (getRandomNumber() * this.androidenAnzahl) / trueSchiffsstrukturen;
        }
        else{
            ergebnis = (getRandomNumber() * anzahlAndroiden) / trueSchiffsstrukturen;
        }

        if (schutzschilde){
            this.schildeInProzent += ergebnis;
        }
        if (energieversorgun){
            this.energieversorgungInProzent += ergebnis;
        }
        if (schiffshuelle){
            this.huelleInProzent += ergebnis;
        }
    }

    public void zustandRaumschiff(){
        System.out.println("<<<< ZUSTAND DES RAUMSCHIFFES >>>>");
        System.out.println();
        System.out.println("Verfügbare Photonentorpedos: " + this.photonentorpedoAnzahl);
        System.out.println("Verfügbare Androiden: " + this.androidenAnzahl);
        System.out.println("Energieversorgung: " + this.energieversorgungInProzent + "%");
        System.out.println("Schilde: " + this.schildeInProzent + "%");
        System.out.println("Hülle: " + this.huelleInProzent + "%");
        System.out.println("Lebenserhaltungssysteme: " + this.lebenserhaltungssystemeInProzent + "%");
        System.out.println();
        System.out.println("<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>");
    }


    /**
     * @return int
     */
    private int getRandomNumber(){
        Random random = new Random();
        return random.nextInt(100 - 0) + 0;
    }
    //#endregion
}
