import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
		String artikel;
		int anzahl;
		double preis;
		double mwst;
		double nettogesamtpreis;
		double bruttogesamtpreis;

		artikel = LiesString("Was m�chten sie bestellen? ");
		anzahl = LiesInt("Geben Sie die Anzahl ein: ");	
		preis = LiesDouble("Geben Sie den Nettopreis in EURO ein: ");
		mwst = LiesMwst("Geben Sie den Mehrwertsteuersatz in Prozent ein: ");
		
		nettogesamtpreis = BerechneGesamtnettopreis(anzahl, preis);
		bruttogesamtpreis = BerechneGesamtbruttopreis(nettogesamtpreis, mwst);
		
		RechnungAusgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}
	
	
	public static String LiesString(String text)
	{	
		Scanner myScanner = new Scanner(System.in);
		System.out.print(text);
		String artikel = myScanner.next();
		return artikel;
	}
	
	public static int LiesInt(String text)
	{	
		Scanner myScanner = new Scanner(System.in);
		System.out.print(text);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}
	
	public static double LiesDouble(String text)
	{	
		Scanner myScanner = new Scanner(System.in);
		System.out.print(text);
		double betrag = myScanner.nextDouble();
		return betrag;
	}
	
	public static double LiesMwst(String text)
	{	
		Scanner myScanner = new Scanner(System.in);
		System.out.print(text);
		double mwst = myScanner.nextDouble();
		mwst = (mwst / 100) + 1;
		myScanner.close();
		return mwst;
	}
	
	
	public static double BerechneGesamtnettopreis(double anzahl, double nettopreis)
	{
		double gesamtNettopreis = anzahl * nettopreis;
		return gesamtNettopreis;
	}
	
	public static double BerechneGesamtbruttopreis(double nettogesamtpreis, double mwst)
	{
		double gesamtBruttopreis = nettogesamtpreis * mwst;
		return gesamtBruttopreis;
	}
	
	public static void RechnungAusgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst)
	{
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst * 100 - 100, "%");
	}

}