import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine r�mische Zahl ein: ");
		
		String eingabe = scanner.next();
		String romZahl = "";
		int[] dezimalZahl = new int[eingabe.length()];
		int dezimalZahlErgebnis = 0;
		
		
		for(int i = 0; i < eingabe.length(); i++) {
			
			switch(eingabe.toUpperCase().charAt(i)) {
			
			case 'I':
				if(!romZahl.toUpperCase().endsWith("III"))
				{
					romZahl += eingabe.toUpperCase().charAt(i);
					dezimalZahl[i] += 1;
				}
				break;
				
			case 'V':
				if(!romZahl.toUpperCase().endsWith("V"))
				{
					if(romZahl.toUpperCase().endsWith("I"))
					{
						romZahl += eingabe.toUpperCase().charAt(i);
						dezimalZahl[i] += 3;
					}
					else
					{
						romZahl += eingabe.toUpperCase().charAt(i);
						dezimalZahl[i] += 5;
					}
				}
				break;
				
			case 'X':
				if(!romZahl.toUpperCase().endsWith("XXX"))
				{
					if(romZahl.toUpperCase().endsWith("I"))
					{
						romZahl += eingabe.toUpperCase().charAt(i);
						dezimalZahl[i] += 8;
					}
					else
					{
						romZahl += eingabe.toUpperCase().charAt(i);
						dezimalZahl[i] += 10;
					}
				}
				break;
				
			case 'L':
				if(!romZahl.toUpperCase().endsWith("L"))
				{

					if(romZahl.toUpperCase().endsWith("X"))
					{
						romZahl += eingabe.toUpperCase().charAt(i);
						dezimalZahl[i] += 30;
					}
					else
					{
						romZahl += eingabe.toUpperCase().charAt(i);
						dezimalZahl[i] += 50;
					}
				}
				break;
				
			case 'C':
				if(!romZahl.toUpperCase().endsWith("CCC"))
				{
					if(romZahl.toUpperCase().endsWith("X"))
					{
						romZahl += eingabe.toUpperCase().charAt(i);
						dezimalZahl[i] += 80;
					}
					else
					{
						romZahl += eingabe.toUpperCase().charAt(i);
						dezimalZahl[i] += 100;
					}
				}
				break;
				
			case 'D':
				if(!romZahl.toUpperCase().endsWith("D"))
				{
					if(romZahl.toUpperCase().endsWith("C"))
					{
						romZahl += eingabe.toUpperCase().charAt(i);
						dezimalZahl[i] += 300;
					}
					else
					{
						romZahl += eingabe.toUpperCase().charAt(i);
						dezimalZahl[i] += 500;
					}
				}
				break;
				
			case 'M':
				if(!romZahl.toUpperCase().endsWith("MMM")) //Ohne diese Abfrage funktionieren auch Zahlen gr��er als 3999
				{
					if(romZahl.toUpperCase().endsWith("C"))
					{
						romZahl += eingabe.toUpperCase().charAt(i);
						dezimalZahl[i] += 800;
					}
					else
					{
						romZahl += eingabe.toUpperCase().charAt(i);
						dezimalZahl[i] += 1000;
					}
				}
				break;
			}
		}
		
		
		if(eingabe.length() == romZahl.length())
		{
			for(int i = 0; i < eingabe.length(); i++)
			{
				dezimalZahlErgebnis += dezimalZahl[i];
			}
			System.out.println();
			System.out.println("Die Dezimalzahl lautet: " + dezimalZahlErgebnis);
		}
		else
		{
			System.out.println("Keine g�ltige r�mische Zahl oder gr��er als 3999. Bitte gib eine andere Zahl ein.");
		}
		
		
	}
	
	

}
