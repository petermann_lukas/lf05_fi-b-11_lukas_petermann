import java.util.Scanner;


public class Main {

	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Gib dein gewŁnschtes Jahr ein: ");
		int jahr = scanner.nextInt();
		
		int jahrestage;
		
		if(jahr % 4 == 0) 
		{
			jahrestage = 366;
			
			if(jahr >= 1582)
			{
									
			if(jahr % 100 == 0) 
			{
				jahrestage = 365;
				
				if(jahr % 400 == 0) 
				{
					jahrestage = 365;
				}
			}
			}
			
		}
		else 
		{
			jahrestage = 365;
		}
		
		if(jahrestage > 365) 
		{
			System.out.println(jahr + " ist ein Schaltjahr.");
		}
		else 
		{
			System.out.println(jahr + " ist kein Schaltjahr");
		}
		
		
		scanner.close();
		
	}

}
