public class Main {
    public static void main(String[] args) throws Exception {
        //#region Erstellen der Raumschiffe
        Raumschiff klingonenRaumschiff = new Raumschiff(1, 100,
                100, 100,
                100, 2, "IKS Hegh'ta");

        Ladung ferengiSchneckensaft = new Ladung("Ferengi Schneckensaft", 200);
        Ladung batlethKlingonenSchwert = new Ladung("Bat'leth Klingonen Schwert", 200);
        klingonenRaumschiff.addLadung(ferengiSchneckensaft);
        klingonenRaumschiff.addLadung(batlethKlingonenSchwert);

        Raumschiff romulanerRaumschiff = new Raumschiff(2, 100,
                100, 100,
                100, 2, "IRW Khazara");

        Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
        Ladung roteMarterie = new Ladung("Rote Marterie", 2);
        Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);
        romulanerRaumschiff.addLadung(borgSchrott);
        romulanerRaumschiff.addLadung(roteMarterie);
        romulanerRaumschiff.addLadung(plasmaWaffe);

        Raumschiff vulkanierRaumschiff = new Raumschiff(0, 80,
                80, 50,
                100, 5, "Ni'Var");

        Ladung forschugssonde = new Ladung("Forschungssonde", 35);
        Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);
        vulkanierRaumschiff.addLadung(forschugssonde);
        vulkanierRaumschiff.addLadung(photonentorpedo);
        //#endregion

        //#region Spielablauf
        klingonenRaumschiff.photonentorpedoSchiessen(romulanerRaumschiff);
        romulanerRaumschiff.phaserkanoneSchiessen(klingonenRaumschiff);
        vulkanierRaumschiff.nachrichtAnAlle("Gewalt ist nicht logisch");
        klingonenRaumschiff.zustandRaumschiff();
        klingonenRaumschiff.ladungsverzeichnisAusgeben();
        vulkanierRaumschiff.reperaturDurchfuehren(true, true, true, vulkanierRaumschiff.getAndroidenAnzahl());
        vulkanierRaumschiff.photonentorpedosLaden();
        vulkanierRaumschiff.ladungsverzeichnisAufraeumen();
        klingonenRaumschiff.photonentorpedoSchiessen(romulanerRaumschiff);
        klingonenRaumschiff.photonentorpedoSchiessen(romulanerRaumschiff);
        klingonenRaumschiff.zustandRaumschiff();
        romulanerRaumschiff.zustandRaumschiff();
        vulkanierRaumschiff.zustandRaumschiff();
        vulkanierRaumschiff.eintraegeLogbuchZurueckgeben();
        romulanerRaumschiff.eintraegeLogbuchZurueckgeben();
        klingonenRaumschiff.eintraegeLogbuchZurueckgeben();
        //#endregion
    }
}
