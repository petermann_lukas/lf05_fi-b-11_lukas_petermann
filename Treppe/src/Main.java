import java.text.MessageFormat;
import java.util.Scanner;


public class Main {

	public static void main(String[] args)
	{
		int h = 7;
		int b = 2;
		int format = 2 * (b*h);
		String stringFormat = MessageFormat.format("%{0}s", format);
		String treppe = "";
		String stufe = "";
		for(int s = 0; s < b; s++)
		{
			stufe += "* ";
		}
		treppe = stufe;
		
		for(int i = 0; i < h; i++)
		{				
			System.out.println(String.format(stringFormat, treppe));
			System.out.println();
			
			if(i <= h)
			{
				treppe += stufe;
			}
		}
	}

}
