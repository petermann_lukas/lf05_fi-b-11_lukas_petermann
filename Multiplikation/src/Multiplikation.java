import java.util.Scanner;

public class Multiplikation {

	public static void main(String[] args)
	{
		double zahl1 = 0.0;
		double zahl2 = 0.0;
		double erg = 0.0;
		
		Programmhinweis();
		zahl1 = Eingabe("1. Zahl: ");
		zahl2 = Eingabe("2. Zahl: ");
		erg = Verarbeitung(zahl1, zahl2);
		Ausgabe(zahl1, zahl2, erg);
	}
	public static Scanner myScanner = new Scanner(System.in);
	
	public static void Programmhinweis()
	{
        System.out.println("Hinweis: ");
        System.out.println("Das Programm multipliziert 2 eingegebene Zahlen. ");
	}
	
	public static double Eingabe(String text)
	{
		System.out.print(text);
		double zahl = myScanner.nextDouble();
		return zahl;
	}
	
	public static double Verarbeitung(double zahl1, double zahl2)
	{
		double erg = zahl1 * zahl2;
		return erg;
	}
	
	public static void Ausgabe(double zahl1, double zahl2, double erg)
	{
        System.out.println("Ergebnis der Multiplikation: ");
        System.out.printf("%.2f * %.2f = %.2f%n", zahl1, zahl2, erg);
	}

}
