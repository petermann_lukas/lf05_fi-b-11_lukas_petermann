import java.util.Scanner;



public class Main {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		int zahl1;
		int zahl2;
		int zahl3;
		
		System.out.print("Gib die erste Zahl ein: ");
		zahl1 = scanner.nextInt();
		
		System.out.print("Gib die zweite Zahl ein: ");
		zahl2 = scanner.nextInt();
		
		System.out.print("Gib die dritte Zahl ein: ");
		zahl3 = scanner.nextInt();
		
		
		if (zahl1 > zahl2 && zahl1 > zahl3 && zahl2 > zahl3) {
			
			System.out.println(zahl1);
			System.out.println(zahl2);
			System.out.println(zahl3);
			
		}
		else if (zahl1 > zahl2 && zahl1 > zahl3 && zahl3 > zahl2) {
			
			System.out.println(zahl1);
			System.out.println(zahl2);
			System.out.println(zahl3);
			
		}
		else if (zahl2 > zahl1 && zahl2 > zahl3 && zahl1 > zahl3) {
			
			System.out.println(zahl2);
			System.out.println(zahl1);
			System.out.println(zahl3);
		}
		else if (zahl2 > zahl1 && zahl2 > zahl3 && zahl3 > zahl1) {
			
			System.out.println(zahl2);
			System.out.println(zahl1);
			System.out.println(zahl3);
		}
		else if (zahl3 > zahl2 && zahl3 > zahl1 && zahl1 > zahl2) {
			
			System.out.println(zahl3);
			System.out.println(zahl1);
			System.out.println(zahl2);
		}
		else if (zahl3 > zahl2 && zahl3 > zahl1 &&  zahl2 > zahl1) {
			
			System.out.println(zahl3);
			System.out.println(zahl2);
			System.out.println(zahl1);
		}
		
		
		else {
			System.out.println("Bitte nur unterscheidliche Zahlen eingeben.");
		}
		
		scanner.close();
	}

}
